﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EntitiyLayer;

namespace StokTakip
{
    public partial class KullaniciList : Form
    {
        public KullaniciList()
        {
            InitializeComponent();
            this.Load += KullaniciList_Load;
        }
        KullaniciManager km = new KullaniciManager();

        private void KullaniciList_Load(object sender, EventArgs e)
        {
           List<Kullanicilar> kullanicilar =  km.GetAll();

            kullaniciGridView.DataSource = kullanicilar;
        }
    }
}
