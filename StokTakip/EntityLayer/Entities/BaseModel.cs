﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiyLayer
{
    public class BaseModel
    {
        [Key]
        public int id { get; set; }
        public string Kodu { get; set; }
        public string Adi { get; set; }
        public DateTime GuncelleTarihi { get; set; }
        public bool silinmis { get; set; }
        public bool pasif { get; set; }
    }
}
