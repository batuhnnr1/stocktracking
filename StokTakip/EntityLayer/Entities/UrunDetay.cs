﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiyLayer
{
    public class UrunDetay : BaseModel
    {
        public Urunler urunler { get; set; }
        public int StokSayisi { get; set; }
        public int SatisFiyati { get; set; }
        public Markalar markalar { get; set; }

    }
}
