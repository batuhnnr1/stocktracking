﻿using DataAccessLayer.Abstract;
using EntitiyLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class KullaniciManager
    {
        Repository<Kullanicilar> kullanicirepo = new Repository<Kullanicilar>();
        public List<Kullanicilar> GetAll()
        {
            return kullanicirepo.List();
        }
    }
}
