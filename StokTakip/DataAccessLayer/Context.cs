﻿using EntitiyLayer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class Context : DbContext
    {
        public DbSet<AltKategori> AltKategori { get; set; }
        public DbSet<Kategoriler> Kategoriler { get; set; }
        public DbSet<Kullanicilar> Kullanicilar { get; set; }
        public DbSet<Markalar> Markalar { get; set; }
        public DbSet<UrunDetay> UrunDetay { get; set; }
        public DbSet<Urunler> Urunler { get; set; }
    }
}