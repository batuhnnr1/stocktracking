﻿namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class stoktakip_mig_001 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AltKategoris",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Kodu = c.String(),
                        Adi = c.String(),
                        GuncelleTarihi = c.DateTime(nullable: false),
                        silinmis = c.Boolean(nullable: false),
                        pasif = c.Boolean(nullable: false),
                        kategoriler_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Kategorilers", t => t.kategoriler_id)
                .Index(t => t.kategoriler_id);
            
            CreateTable(
                "dbo.Kategorilers",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Kodu = c.String(),
                        Adi = c.String(),
                        GuncelleTarihi = c.DateTime(nullable: false),
                        silinmis = c.Boolean(nullable: false),
                        pasif = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Kullanicilars",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Soyadi = c.String(),
                        Sifre = c.String(),
                        Kodu = c.String(),
                        Adi = c.String(),
                        GuncelleTarihi = c.DateTime(nullable: false),
                        silinmis = c.Boolean(nullable: false),
                        pasif = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Markalars",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Kodu = c.String(),
                        Adi = c.String(),
                        GuncelleTarihi = c.DateTime(nullable: false),
                        silinmis = c.Boolean(nullable: false),
                        pasif = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.UrunDetays",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        StokSayisi = c.Int(nullable: false),
                        SatisFiyati = c.Int(nullable: false),
                        Kodu = c.String(),
                        Adi = c.String(),
                        GuncelleTarihi = c.DateTime(nullable: false),
                        silinmis = c.Boolean(nullable: false),
                        pasif = c.Boolean(nullable: false),
                        markalar_id = c.Int(),
                        urunler_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Markalars", t => t.markalar_id)
                .ForeignKey("dbo.Urunlers", t => t.urunler_id)
                .Index(t => t.markalar_id)
                .Index(t => t.urunler_id);
            
            CreateTable(
                "dbo.Urunlers",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Kodu = c.String(),
                        Adi = c.String(),
                        GuncelleTarihi = c.DateTime(nullable: false),
                        silinmis = c.Boolean(nullable: false),
                        pasif = c.Boolean(nullable: false),
                        kategoriler_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Kategorilers", t => t.kategoriler_id)
                .Index(t => t.kategoriler_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UrunDetays", "urunler_id", "dbo.Urunlers");
            DropForeignKey("dbo.Urunlers", "kategoriler_id", "dbo.Kategorilers");
            DropForeignKey("dbo.UrunDetays", "markalar_id", "dbo.Markalars");
            DropForeignKey("dbo.AltKategoris", "kategoriler_id", "dbo.Kategorilers");
            DropIndex("dbo.Urunlers", new[] { "kategoriler_id" });
            DropIndex("dbo.UrunDetays", new[] { "urunler_id" });
            DropIndex("dbo.UrunDetays", new[] { "markalar_id" });
            DropIndex("dbo.AltKategoris", new[] { "kategoriler_id" });
            DropTable("dbo.Urunlers");
            DropTable("dbo.UrunDetays");
            DropTable("dbo.Markalars");
            DropTable("dbo.Kullanicilars");
            DropTable("dbo.Kategorilers");
            DropTable("dbo.AltKategoris");
        }
    }
}
